import React from 'react';
import './App.css';
import './Parent.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
   
      <h1>"Hello {names}, I am  the child component!"</h1>

      </header>
    </div>
  );
}

export default App;
